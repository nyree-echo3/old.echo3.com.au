<?php
//dd($errors);
?>
<h3>Billing Address</h3>

<div class="form-group form-group-sm row{{ $errors->has('billpayer.address.country_id') ? ' has-danger' : '' }} hidden-country">
    <label class="form-control-label col-md-2">{{ __('Country') }}</label>
    <div class="col-md-10">
        
        {{ Form::select('billpayer[address][country_id]', $countries->pluck('name', 'id'), 'NL', ['class' => 'form-control']) }}

        @if ($errors->has('billpayer.address.country_id'))
            <div class="form-control-feedback">{{ $errors->first('billpayer.address.country_id') }}</div>
        @endif
    </div>
</div>

<div class="form-group form-group-sm row{{ $errors->has('billpayer.address.address') ? ' has-danger' : '' }}">
    <label class="form-control-label col-md-2">{{ __('Address *') }}</label>
    <div class="col-md-10">
        {{ Form::text('billpayer[address][address]', null, ['class' => 'form-control', 'placeholder' => 'Address (Line 1)']) }}
        
        @if ($errors->has('billpayer.address.address'))
            <div class="form-control-feedback"><i class="fas fa-exclamation"></i> Address is required <!-- {{ $errors->first('billpayer.address.address') }} --></div>
        @endif
    </div>
</div>

<div class="form-group form-group-sm row{{ $errors->has('billpayer.address.address2') ? ' has-danger' : '' }}">
    <label class="form-control-label col-md-2"></label>
    <div class="col-md-10">
        {{ Form::text('billpayer[address][address2]', null, ['class' => 'form-control', 'placeholder' => 'Address (Line 2)']) }}
        
        @if ($errors->has('billpayer.address.address2'))
            <div class="form-control-feedback"><i class="fas fa-exclamation"></i> {{ $errors->first('billpayer.address.address2') }}</div>
        @endif
    </div>
</div>

<div class="form-group form-group-sm row{{ $errors->has('billpayer.address.postalcode') ? ' has-danger' : '' }}">    
    <label class="form-control-label col-md-2">{{ __('Suburb *') }}</label>
    <div class="col-md-10">
        {{ Form::text('billpayer[address][city]', null, ['class' => 'form-control', 'placeholder' => 'Suburb']) }}
        
        @if ($errors->has('billpayer.address.city'))
            <div class="form-control-feedback"><i class="fas fa-exclamation"></i> Suburb is required <!-- {{ $errors->first('billpayer.address.city') }} --></div>
        @endif
    </div>
</div>

<div class="form-group form-group-sm row{{ $errors->has('billpayer.address.province_id') ? ' has-danger' : '' }}">              
    <label class="form-control-label col-md-2">{{ __('State *') }}</label>
    <div class="col-md-10">
         {{ Form::select('billpayer[address][province_id]', $states->pluck('state', 'id'), 'NL', ['class' => 'form-control']) }}
         
         @if ($errors->has('billpayer.address.province_id'))
            <div class="form-control-feedback"><i class="fas fa-exclamation"></i> State is required <!-- {{ $errors->first('billpayer.address.province_id') }} --></div>
        @endif
    </div>
</div>
    
<div class="form-group form-group-sm row{{ $errors->has('billpayer.address.address') ? ' has-danger' : '' }}">    
    <label class="form-control-label col-md-2">{{ __('Postcode *') }}</label>
    <div class="col-md-10">
        {{ Form::text('billpayer[address][postalcode]', null, ['class' => 'form-control', 'placeholder' => 'Postcode']) }}
        
        @if ($errors->has('billpayer.address.postalcode'))
            <div class="form-control-feedback"><i class="fas fa-exclamation"></i> Postcode is required <!-- {{ $errors->first('billpayer.address.postalcode') }} --></div>
        @endif
    </div>
</div>

