@extends('site/layouts/app')

@section('content')

@include('site/partials/home-video')

<!-- Intro Text -->
 <div class="container marketing">
	 <div class="row featurette">
	     <div class="col home-intro">
		<?php echo $home_intro_text; ?>
	     </div>
	</div>
</div>

<!--
@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')
-->

@endsection
