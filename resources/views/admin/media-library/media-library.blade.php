@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">

        <iframe id="ck" src="{{ url('/') }}/components/ckfinder/ckfinder.html?type=Media&CKEditor=body&CKEditorFuncNum=1&langCode=en-gb" style="height: 750px; width: 100%; border: none"></iframe>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            iframeheight();
        });
        $( window ).resize(function() {
            iframeheight();
        });

        function iframeheight(){
            $('#ck').height( $(window).height()-110);
        }
    </script>
@endsection