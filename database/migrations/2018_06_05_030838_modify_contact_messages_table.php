<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyContactMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_messages', function (Blueprint $table) {
            $table->dropColumn([
                'surname',
                'phone',
                'email',
                'address',
                'suburb',
                'state',
                'postcode',
                'preferred_contact',
                'enquiry_type',
                'h_find_us',
                'enquiry_details',
                'favourite'
            ]);

            $table->text('message')->nullable()->after('name');
            $table->text('data')->nullable()->after('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_messages', function (Blueprint $table) {
            //
        });
    }
}
