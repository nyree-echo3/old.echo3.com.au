<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('settings')->insert([
            'key' => 'home-intro-text',
            'value' => 'home intro text',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('settings')->insert([
            'key' => 'meta-title',
            'value' => 'meta title',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('settings')->insert([
            'key' => 'meta-keywords',
            'value' => 'meta keywords',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('settings')->insert([
            'key' => 'meta-description',
            'value' => 'meta description',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('settings')->insert([
            'key' => 'contact-details',
            'value' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'company-name',
            'value' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'phone-number',
            'value' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('settings')->insert([
            'key' => 'fax-number',
            'value' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('settings')->insert([
            'key' => 'address',
            'value' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'live-date',
            'value' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'google-analytics',
            'value' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'social-facebook',
            'value' => 'https://facebook.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'social-twitter',
            'value' => 'https://twitter.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'social-linkedin',
            'value' => 'http://www.linkedin.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'social-googleplus',
            'value' => 'http://google.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'social-instagram',
            'value' => 'https://instagram.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'social-pinterest',
            'value' => 'https://pintrest.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('settings')->insert([
            'key' => 'social-youtube',
            'value' => 'https://youtube.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('settings')->insert([
            'key' => 'contact-form-fields',
            'value' => '[{"type":"text","required":true,"label":"Name","className":"form-control","name":"name","subtype":"text"},{"type":"text","subtype":"email","required":true,"label":"Email","className":"form-control","name":"email"},{"type":"textarea","required":true,"label":"Message","className":"form-control","name":"message","subtype":"textarea"}]',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('settings')->insert([
            'key' => 'email',
            'value' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

		DB::table('settings')->insert([
            'key' => 'contact-email',
            'value' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
