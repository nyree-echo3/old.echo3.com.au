<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (Auth::guest()) {
            abort(403);
        }

        if($permission == "edit-user"){
            if ($request->user()->can('edit-permission') || $request->user()->can('user-change-password')) {
                return $next($request);
            }
        }

        if (! $request->user()->can($permission)) {
            return \Redirect::to('dreamcms/dashboard')->with('message', Array('text' => 'Access Denied!', 'status' => 'error'));
        }

        return $next($request);
    }
}
